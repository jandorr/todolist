
exports.getDateString = function () {
    const options = {
        weekday: 'long',
        day: 'numeric',
        month: 'long',
    };
    return new Date().toLocaleDateString('en-US', options);
}

exports.getDayString = function () {
    const options = {
        weekday: 'long'
    };
    return new Date().toLocaleDateString('en-US', options);
}
