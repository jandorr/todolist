const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const _ = require('lodash');
const date = require(__dirname + '/date.js');

// Set port
let port = process.env.PORT;
if (port == null || port == '') {
    port = 3000;
}

// Assigning a new array to a const is not possible but pushing items is fine.
const workItems = [];

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

// Database Setup
let dbUrl =
    'mongodb+srv://admin-jan:' +
    process.env.DB_PASSWORD +
    '@cluster0.js8fa.mongodb.net/todolistDB';
mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true });

// Item model
const itemsSchema = mongoose.Schema({
    text: {
        type: String,
        required: true,
    },
});
const Item = mongoose.model('Item', itemsSchema);

// List model
const listSchema = mongoose.Schema({
    name: String,
    items: [itemsSchema],
});
const List = mongoose.model('List', listSchema);

// Create default items
const item1 = new Item({ text: 'Welcome!' });
const item2 = new Item({ text: 'Click + to Add' });
const item3 = new Item({ text: 'Click - to Delete' });
const defaultItems = [item1, item2, item3];

app.set('view engine', 'ejs');

// To Do List
app.get('/', function (req, res) {
    Item.find({}, function (err, foundItems) {
        if (err) {
            console.log(err);
        } else {
            if (foundItems.length === 0) {
                Item.insertMany(defaultItems, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Inserted default items.');
                    }
                });

                res.redirect('/'); // This line is not necessary.
            } else {
                // Will render views/list.ejs using EJS templating.
                res.render('list', {
                    listTitle: 'Today',
                    listItems: foundItems,
                });
            }
        }
    });
});

app.post('/', function (req, res) {
    const newItem = req.body.newItem;
    const list = req.body.list;

    const item = new Item({ text: newItem });

    if (list === 'Today') {
        item.save();
        res.redirect('/');
    } else {
        List.findOne({ name: list }, function (err, foundList) {
            foundList.items.push(item);
            foundList.save();
            res.redirect('/' + list);
        });
    }
});

app.post('/delete', function (req, res) {
    const checkedItemId = req.body.checkbox;
    const listName = req.body.listName;
    console.log(listName);

    if (listName === 'Today') {
        Item.findByIdAndDelete(checkedItemId, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('Deleted item with ID: ' + checkedItemId);
                res.redirect('/');
            }
        });
    } else {
        List.findOneAndUpdate(
            { name: listName },
            { $pull: { items: { _id: checkedItemId } } },
            function (err, foundList) {
                if (!err) {
                    res.redirect('/' + listName);
                }
            }
        );
    }
});

// Custom List
const customLists = ['Work', 'School', 'Home'];

app.get('/:customListName', function (req, res) {
    const customListName = _.capitalize(req.params.customListName);

    List.findOne({ name: customListName }, function (err, foundList) {
        if (err) {
            console.log(err);
        } else {
            if (foundList) {
                res.render('list', {
                    listTitle: foundList.name,
                    listItems: foundList.items,
                });
            } else {
                // Check if customListName is in customLists.
                if (customLists.indexOf(customListName) > -1) {
                    // Create a new list.
                    const list = new List({
                        name: customListName,
                        items: defaultItems,
                    });
                    list.save();
                    console.log('Saved new ' + customListName);
                    res.redirect('/' + customListName);
                } else {
                    console.log(customListName + ' not in ' + customLists);
                }
            }
        }
    });
});

// About page
app.get('/about', function (req, res) {
    res.render('about');
});

app.listen(port, function () {
    console.log(`Server started on port ${port}`);
});
